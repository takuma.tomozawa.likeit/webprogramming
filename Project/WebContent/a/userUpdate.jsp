<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ新規登録</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<header>
		<div align="right">
		<span style="margin-right:100px">
		ユーザー名
		</span>
		<span style="margin-right:100px">
		<a href="login.html">
		ログアウト
		</a>
		</span>
		</div>
	</header>
	<div class="form-area">
	<div align="center">
	<h1>ユーザ情報更新</h1>
	</div>
	<br>
	<div class="row">
	<div class="col-sm-6">
	<b>ログインID</b>
	</div>
	<div class="col-sm-6">
	<input type="text" name="id" style="width:200px;" class="form-control">
	</div>
	</div>
	<br>
	<div class="row">
	<div class="col-sm-6">
	<b>パスワード</b>
	</div>
	<div class="col-sm-6">
	<input type="text" name="pass" style="width:200px;" class="form-control">
	</div>
	</div>
	<br>
	<div class="row">
	<div class="col-sm-6">
	<b>パスワード（確認）</b>
	</div>
	<div class="col-sm-6">
	<input type="text" name="pass" style="width:200px;" class="form-control">
	</div>
	</div>
	<br>
	<div class="row">
	<div class="col-sm-6">
	<b>ユーザ名</b>
	</div>
	<div class="col-sm-6">
	<input type="text" name="name" style="width:200px;" class="form-control">
	</div>
	</div>
	<br>
	<div class="row">
	<div class="col-sm-6">
	<b>生年月日</b>
	</div>
	<div class="col-sm-6">
	<input type="date" name="birth" style="width:200px;" class="form-control">
	</div>
	</div>
	<br>
	<div align="center">
	<input type="submit" style="width:100px;" value="更新">
	</div>
	<br>
	<a href="userList.html">戻る</a>
	</div>
</body>
</html>