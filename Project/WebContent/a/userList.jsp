<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ一覧</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style2.css">
</head>
<body>
	<header>
		<div align="right">
		<span style="margin-right:100px">
		ユーザー名
		</span>
		<span style="margin-right:100px">
		<a href="login.html">
		ログアウト
		</a>
		</span>
		</div>
	</header>
	<div class="form-area">
		<div align="center">
		<h1>ユーザ一覧</h1>
		</div>
		<br>
		<div align="right">
		<a href="newUser.html">
		新規登録
		</a>
		</div>
		<br>
		<div class="row">
		<div class="col-sm-3">
		<b>ログインID</b>
		</div>
		<div align="center">
		<input type="text" name="id" style="width:350px;" class="form-control">
		</div>
		</div>
		<br>
		<div class="row">
		<div class="col-sm-3">
		<b>ユーザ名</b>
		</div>
		<div align="center">
		<input type="text" name="pass" style="width:350px;" class="form-control">
		</div>
		</div>
		<br>
		<div class="row">
		<div class="col-sm-3">
		<b>生年月日</b>
		</div>
		<div align="center">
		<input type="date" name="example1">
		～
		<input type="date" name="example2">
		</div>
		</div>
		<div align="right"><input type="submit" value="検索"></div>
		</div>

	<hr noshade>
	<div align="center">
		<table border="2" width="600">
		<tr bgcolor="lavender">
		<th>ログインID</th>
		<th>ユーザ名</th>
		<th>生年月日</th>
		<th></th>
		</tr>
		<tr>
		<td>id0001</td>
		<td>田中太郎</td>
		<td>1989年04月26日</td>
		<td>
		<span style="margin-left:10px">
		<a href="userDetails.html">
		<input type="submit" value="詳細" style="background-color: #1e90ff;color: #ffffff">
		</a>
		</span>
		<span style="margin-left:10px">
		<a href="userUpdate.html">
		<input type="submit" value="更新" style="background-color: #2e8b57;color: #ffffff">
		</a>
		</span>
		<span style="margin-left:10px">
		<a href="userDelete.html">
		<input type="submit" value="削除" style="background-color: #ff7f50;color: #ffffff">
		</a>
		</span>
		</td>
		</tr>
		<tr>
		<td>id0002</td>
		<td>佐藤二郎</td>
		<td>2001年11月12日</td>
		<td>
		<span style="margin-left:10px">
		<a href="userDetails.html">
		<input type="submit" value="詳細" style="background-color: #1e90ff;color: #ffffff">
		</a>
		</span>
		<span style="margin-left:10px">
		<a href="userUpdate.html">
		<input type="submit" value="更新" style="background-color: #2e8b57;color: #ffffff">
		</a>
		</span>
		<span style="margin-left:10px">
		<a href="userDelete.html">
		<input type="submit" value="削除" style="background-color: #ff7f50;color: #ffffff">
		</a>
		</span>
		</td>
		</tr>
		<tr>
		<td>id0003</td>
		<td>佐川真司</td>
		<td>2000年01月01日</td>
		<td>
		<span style="margin-left:10px">
		<a href="userDetails.html">
		<input type="submit" value="詳細" style="background-color: #1e90ff;color: #ffffff">
		</a>
		</span>
		<span style="margin-left:10px">
		<a href="userUpdate.html">
		<input type="submit" value="更新" style="background-color: #2e8b57;color: #ffffff">
		</a>
		</span>
		<span style="margin-left:10px">
		<a href="userDelete.html">
		<input type="submit" value="削除" style="background-color: #ff7f50;color: #ffffff">
		</a>
		</span>
		</td>
		</tr>

		</table>
	</div>
</body>
</html>