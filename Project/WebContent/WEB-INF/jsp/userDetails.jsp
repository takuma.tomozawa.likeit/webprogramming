<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ情報詳細一覧</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
<form action="UserManagement/UserDetailServlet" method="get">
	<header class="bg-secondary text-white">
	<div class="container">
		<div align="right" >
		<span style="margin-right:100px">
		${userInfo.name}
		</span>
		<a href="LogoutServlet">
		ログアウト
		</a>
		</div>
		</div>
	</header>
	<div class="form-area">
	<div align="center">
	<h1>ユーザ情報詳細一覧</h1>
	</div>
	<br>
	<div class="row">
	<div class="col-sm-6">
	<b>ログインID</b>
	</div>
	<div class="col-sm-6">
	${u.loginId}
	</div>
	</div>
	<br>
	<div class="row">
	<div class="col-sm-6">
	<b>ユーザ名</b>
	</div>
	<div class="col-sm-6">
	${u.name}
	</div>
	</div>
	<br>
	<div class="row">
	<div class="col-sm-6">
	<b>生年月日</b>
	</div>
	<div class="col-sm-6">
	${u.birthDate}
	</div>
	</div>
	<br>
	<div class="row">
	<div class="col-sm-6">
	<b>登録日時</b>
	</div>
	<div class="col-sm-6">
	${u.createDate}
	</div>
	</div>
	<br>
	<div class="row">
	<div class="col-sm-6">
	<b>更新日時</b>
	</div>
	<div class="col-sm-6">
	${u.updateDate}
	</div>
	</div>
	<br>

	<a href="UserListServlet">戻る</a>
	</div>
</form>
</body>
</html>