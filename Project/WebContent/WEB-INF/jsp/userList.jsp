<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ一覧</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style2.css">
</head>
	<body>


		<header class="bg-secondary text-white">
			<div class="container">
				<div align="right" >
					<span style="margin-right:100px">
					${userInfo.name}
					</span>
					<a href="LogoutServlet">
					ログアウト
					</a>
				</div>
			</div>
		</header>
		<form action="/UserManagement/UserListServlet" method="post">

			<div class="form-area">
				<div align="center">
					<h1>ユーザ一覧</h1>
				</div>
				<br>
				<div align="right">
					<a href="UserCreateServlet">
					新規登録
					</a>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-3">
						<b>ログインID</b>
					</div>
					<div align="center">
						<input type="text" name="loginId" style="width:350px;" class="form-control">
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-3">
						<b>ユーザ名</b>
					</div>
					<div align="center">
						<input type="text" name="name" style="width:350px;" class="form-control">
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-3">
						<b>生年月日</b>
					</div>
					<div align="center">
						<input type="date" name="birth1">
						～
						<input type="date" name="birth2">
					</div>
				</div>

				<div align="right">
					<a href="UserListServlet">
						<input type="submit" value="検索">
					</a>
				</div>
			</div>


		<hr noshade>
			<div align="center">
				<table border="2" width="600">
					<tr bgcolor="lavender">
						<th>ログインID</th>
						<th>ユーザ名</th>
						<th>生年月日</th>
						<th></th>
					</tr>

					<c:if test="${s != null}">
					<c:forEach var="user" items="${s}" >
						<tr>
							<td>${user.loginId}</td>
							<td>${user.name}</td>
							<td>${user.birthDate}</td>
							<td>

							<span style="margin-left:10px">
								<a href="UserDetailServlet?id=${user.id}">
									<input type="button" value="詳細" style="background-color: #1e90ff;color: #ffffff">
								</a>
							</span>

							<%--jspでの文字列の比較は==でよい --%>
							<c:if test="${userInfo.loginId == user.loginId && userInfo.loginId != 'admin'}">
								<span style="margin-left:10px">
									<a href="UserUpdateServlet?id=${user.id}">
										<input type="button" value="更新" style="background-color: #2e8b57;color: #ffffff">
									</a>
								</span>
							</c:if>
							<c:if test="${userInfo.loginId == 'admin'}">
								<span style="margin-left:10px">
									<a href="UserUpdateServlet?id=${user.id}">
										<input type="button" value="更新" style="background-color: #2e8b57;color: #ffffff">
									</a>
								</span>
							</c:if>

							<%--userとuserInfoという物がありuserInfoはログインした情報が入っているからそこからとってくる--%>
							<c:if test="${userInfo.loginId == 'admin'}">
								<span style="margin-left:10px">
									<a href="UserDeleteServlet?id=${user.id}">
										<input type="button" value="削除" style="background-color: #ff7f50;color: #ffffff">
									</a>
								</span>
							</c:if>

							</td>
						</tr>
					</c:forEach>
					</c:if>

					<c:if test="${s == null}">
					<c:forEach var="user" items="${userList}" >
						<tr>
							<td>${user.loginId}</td>
							<td>${user.name}</td>
							<td>${user.birthDate}</td>
							<td>

							<span style="margin-left:10px">
								<a href="UserDetailServlet?id=${user.id}">
									<input type="button" value="詳細" style="background-color: #1e90ff;color: #ffffff">
								</a>
							</span>

							<%--jspでの文字列の比較は==でよい --%>
							<c:if test="${userInfo.loginId == user.loginId && userInfo.loginId != 'admin'}">
								<span style="margin-left:10px">
									<a href="UserUpdateServlet?id=${user.id}">
										<input type="button" value="更新" style="background-color: #2e8b57;color: #ffffff">
									</a>
								</span>
							</c:if>
							<c:if test="${userInfo.loginId == 'admin'}">
								<span style="margin-left:10px">
									<a href="UserUpdateServlet?id=${user.id}">
										<input type="button" value="更新" style="background-color: #2e8b57;color: #ffffff">
									</a>
								</span>
							</c:if>

							<%--userとuserInfoという物がありuserInfoはログインした情報が入っているからそこからとってくる--%>
							<c:if test="${userInfo.loginId == 'admin'}">
								<span style="margin-left:10px">
									<a href="UserDeleteServlet?id=${user.id}">
										<input type="button" value="削除" style="background-color: #ff7f50;color: #ffffff">
									</a>
								</span>
							</c:if>

							</td>
						</tr>
					</c:forEach>
					</c:if>

					<tr>

				</table>
			</div>
		</form>
	</body>
</html>