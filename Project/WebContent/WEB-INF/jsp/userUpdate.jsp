<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ新規登録</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<form action="UserUpdateServlet" method="post">
	<input type="hidden" name="id" value="${u.id}">

	<header class="bg-secondary text-white">
	<div class="container">
		<div align="right" >
		<span style="margin-right:100px">
		${userInfo.name}
		</span>
		<a href="LogoutServlet">
		ログアウト
		</a>
		</div>
		</div>
	</header>

	<div class="form-area">
	<div align="center">
	<h1>ユーザ情報更新</h1>
	</div>

		<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>

	<br>
	<div class="row">
	<div class="col-sm-6">
	<b>ログインID</b>
	</div>
	<div class="col-sm-6">
	${u.loginId}
	</div>
	</div>
	<br>
	<div class="row">
	<div class="col-sm-6">
	<b>パスワード</b>
	</div>
	<div class="col-sm-6">
	<input type="password" name="password" style="width:200px;" class="form-control">
	</div>
	</div>
	<br>
	<div class="row">
	<div class="col-sm-6">
	<b>パスワード（確認）</b>
	</div>
	<div class="col-sm-6">
	<input type="password" name="password2" style="width:200px;" class="form-control">
	</div>
	</div>
	<br>
	<div class="row">
	<div class="col-sm-6">
	<b>ユーザ名</b>
	</div>
	<div class="col-sm-6">
	<input type="text" name="name" style="width:200px;" class="form-control" value="${u.name}">
	</div>
	</div>
	<br>
	<div class="row">
	<div class="col-sm-6">
	<b>生年月日</b>
	</div>
	<div class="col-sm-6">
	<input type="date" name="birth" style="width:200px;" class="form-control" value="${u.birthDate}">
	</div>
	</div>
	<br>
	<div align="center">
	<input type="submit"  value="更新">
	</div>
	<br>
	<a href="UserListServlet">戻る</a>
	</div>
	</form>

</body>
</html>