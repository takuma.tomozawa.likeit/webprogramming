<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ユーザ新規登録</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
</head>
	<body>
		<form action="UserDeleteServlet" method="post">
		<input type="hidden" name="id" value="${u.id}">

		<header class="bg-secondary text-white">
		<div class="container">
			<div align="right" >
			<span style="margin-right:100px">
			${userInfo.name}
			</span>
			<a href="LogoutServlet">
			ログアウト
			</a>
			</div>
			</div>
		</header>

			<div align="right" class="form-area">
				<div align="center">
				<h1>ユーザ削除確認</h1>
				<br>
				<div>ログインID;</div>
				<div>削除</div>
				<br>
				<span style="margin-right:50px">
					<input type="submit" name="cancellation" value="キャンセル">
				</span>
					<input type="submit" name="delete" value="OK">
				</div>
				<br>
				<div align="left">
					<a href="UserListServlet">
					戻る
					</a>
				</div>
			</div>
		</form>
	</body>
</html>