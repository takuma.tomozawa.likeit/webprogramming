<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ログイン画面</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">

</head>
<body>
	<form action="/UserManagement/LoginServlet" method="post">


    <div class="container">

	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
	</div>
	<div class="form-area">
	<div align="center">
	<h1>ログイン画面</h1></div>
	<div class="row">
	<div class="col-sm-6">
	<span style="margin-left:10px"></span><b>ログインID</b>
	</div>
	<div class="col-sm-6">
	<input type="text" name="loginId" style="width:200px;" class="form-control" placeholder="例：admin" value="admin">
	</div>
	</div>
	<br>

	<div class="row">
	<div class="col-sm-6">
	<span style="margin-left:10px"></span><b>パスワード</b>
	</div>
	<div class="col-sm-6">
	<input type="password" name="password" style="width:200px;" class="form-control" placeholder="例：password" value="password">
	</div>
	</div>

	<br>
	<div align="center">
	<a href="UserListServlet">
	<input type="submit" value="ログイン">
	</a>
	</div>
	</div>
	</form>
</body>
</html>