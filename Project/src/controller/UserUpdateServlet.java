package controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.MD5;
import dao.UserDao;
import model.User;

@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		request.setCharacterEncoding("UTF-8");
//		String s = (String)request.getAttribute("userInfo");
//		if(s == null) {
//			response.sendRedirect("LoginServlet");
//			return;
//		}
		HttpSession session = request.getSession();
		User us = (User)session.getAttribute("userInfo");
		if(us == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		String i = request.getParameter("id");
		int id = Integer.parseInt(i);

		UserDao user = new UserDao();
		User u = user.Detail(id);

//		HttpSession session = request.getSession();
		session.setAttribute("u", u);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);



	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birth = request.getParameter("birth");
		String id = request.getParameter("id");

			if(name .equals("") || birth.equals("")) {
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				request.setAttribute("name",name);
				request.setAttribute("birth",birth);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
				dispatcher.forward(request, response);
				return;
			}


			if(!password.equals(password2)) {
				request.setAttribute("errMsg", "パスワードが一致しません");
				request.setAttribute("name",name);
				request.setAttribute("birth",birth);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}
			try {
				MD5 m = new MD5();
				UserDao userDao = new UserDao();


				userDao.update(name, birth, m.getPassword(password), id);
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				}

		response.sendRedirect("UserListServlet");
	}

}
