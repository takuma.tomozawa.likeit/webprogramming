package controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.MD5;
import dao.UserDao;
import model.User;

/**
 * Servlet implementation class loginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public LoginServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//ログインID;adimin パスワード;password
		HttpSession session = request.getSession();
		User us = (User)session.getAttribute("userInfo");
		if(us != null) {
			response.sendRedirect("UserListServlet");
			return;
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		// フォワード
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		//入力された文字をそれぞれに代入
		String loginId = request.getParameter("loginId");

		String password = request.getParameter("password");

		//インスタンスを作成し、入力された文字を渡す
		UserDao userDao = new UserDao();
		//ログイン画面でもMD5でパスワードを暗号化する処理を行う
		MD5 m = new MD5();

		User user = null;

		try {
			user = userDao.findByLoginInfo(loginId, m.getPassword(password));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		//呼び出したメソッドuserに値が入っているかどうかの分岐
		//findByLoginInfoで未入力の個所があるとtry文で捕まりnullを返されるので、以下のif文が実行される
		if(user == null) {
			//セッションにerrMsgという変数でログイン失敗という文字を記録
			request.setAttribute("errMsg", "ログイン失敗");

			//ログインが失敗したらログイン画面を再表示という処理
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
			dispatcher.forward(request, response);
			return;
		}
		HttpSession session = request.getSession();
		//第2引数のuserには入力されたloginIdとpasswordが入ったuserがある
		//セッションにユーザ一覧情報をセット
		session.setAttribute("userInfo", user);
		//UserListServletにリダイレクトする
		response.sendRedirect("UserListServlet");
	}
}
