package controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.MD5;
import dao.RegistrationError;
import dao.UserDao;
import model.User;

@WebServlet("/UserCreateServlet")
public class UserCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public UserCreateServlet() {
        super();
    }

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		User us = (User)session.getAttribute("userInfo");
		if(us == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

//		HttpSession session = request.getSession();
//		if(session == null) {
//			response.sendRedirect("/UserListServlet");
//		}
		request.setCharacterEncoding("UTF-8");
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
		// フォワード
		dispatcher.forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");


		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birth = request.getParameter("birth");
		String create = request.getParameter("create");
		String update = request.getParameter("update");

		RegistrationError r = new RegistrationError();
		//InputCheck ic = new InputCheck();

		String rr = r.registrationEerror(loginId);
			//入力したloginIdを渡し、同名のものがなければこの処理を飛ばす
			if(loginId.equals(rr)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("loginId",loginId);
			request.setAttribute("name",name);
			request.setAttribute("birth",birth);
			request.setAttribute("create",create);
			request.setAttribute("update",update);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
			dispatcher.forward(request, response);
			return ;
		}



			//未入力の項目をチェック
			//空白とnullは別、今回は空白という扱いなので""で比較をする必要がある
			if(loginId.equals("") || password .equals("") || name .equals("") || birth.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			request.setAttribute("loginId",loginId);
			request.setAttribute("name",name);
			request.setAttribute("birth",birth);
			request.setAttribute("create",create);
			request.setAttribute("update",update);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
			dispatcher.forward(request, response);
			return ;
		}

			//入力されたパスワードが同じものかどうか確認する
			if(!password.equals(password2)) {
			request.setAttribute("errMsg", "パスワードが一致しません");
			request.setAttribute("loginId",loginId);
			request.setAttribute("name",name);
			request.setAttribute("birth",birth);
			request.setAttribute("create",create);
			request.setAttribute("update",update);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/newUser.jsp");
			dispatcher.forward(request, response);
			return ;

		}

			//パスワードをMD5で暗号化
			UserDao userDao = new UserDao();
			MD5 m = new MD5();

			//try文自動生成
			try {
				userDao.insert(loginId, name, birth, m.getPassword(password));
			} catch (NoSuchAlgorithmException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
			response.sendRedirect("UserListServlet");
	}

}
