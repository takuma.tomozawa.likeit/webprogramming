package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDetailServlet
 */
@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDetailServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		User us = (User)session.getAttribute("userInfo");
		if(us == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		String i = request.getParameter("id");
//		System.out.println(i);
		int id = Integer.parseInt(i);

		UserDao user = new UserDao();
		User u = user.Detail(id);

//		HttpSession session = request.getSession();
		session.setAttribute("u", u);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetails.jsp");
		dispatcher.forward(request, response);



	}
}
