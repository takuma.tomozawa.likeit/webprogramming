package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RegistrationError {
	public  String registrationEerror(String loginId) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			//String型を表示しないのでどっか修正
			String sql = "SELECT login_id FROM user WHERE login_id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			//UserCreateで入力されたloginIdが第2引数に入る
			pStmt.setString(1, loginId);
			//loginIdの結果表の準備
			ResultSet rs = pStmt.executeQuery();

			while (!rs.next()) {
				return null;
			}


			//結果表のlogin_idを呼び出し、行数分それを実行
			String loginData = rs.getString("login_id");
			return loginData;


		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;

				}
			}
		}
	}

}
