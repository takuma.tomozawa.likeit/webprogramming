package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.User;

/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class UserDao {

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginId
	 * @param password
	 * @return
	 */
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			//実際のログイン画面に入力された値が入る
			//※loginId = admin、password = password
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			//呼び出し元にuser（オブジェクト型）で値を返す
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		//以下は条件に当てはまるものをここでキャッチし呼び出し元にnullを返す
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE id >=2";

			// SELECTを実行し、結果表を取得

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}


	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public List<User> findSearch(String loginIdP,String nameP, String startDate, String endDate) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE id >=2";

			if(!(loginIdP.equals(""))) {
				sql += " AND login_id = '" + loginIdP + "'";
			}
			//部分一致で処理
			if(!(nameP.equals(""))) {
				sql += " AND name LIKE '%" + nameP + "%'";
			}

			if(!(startDate.equals("")) || !(endDate.equals(""))) {
				sql += " AND birth_date BETWEEN '" + startDate + "' AND '" + endDate + "'";
			}

			// SELECTを実行し、結果表を取得

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);//この位置でデバッグする

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;//[]が入っていた
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;//nullのまま
				}
			}
		}
		return userList;
	}

	//ログイン画面のやつと同じようなかんじでINSRET文などで新しくメソッドを作る
	public void insert(String loginId, String name, String birthDate, String password) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			con = DBManager.getConnection();
			String insertSQL = "INSERT INTO user(login_id, name, birth_date, password, create_date, update_date) VALUES(?,?,?,?,now(),now())";

			stmt = con.prepareStatement(insertSQL);

			stmt.setString(1, loginId);
			stmt.setString(2, name);
			stmt.setString(3, birthDate);
			stmt.setString(4, password);
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public User Detail(int id) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setInt(1, id);

			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			int idData = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			Date birthData = rs.getDate("birth_date");
			String passwordData = rs.getString("password");
			String createData = rs.getString("create_date");
			String updataData = rs.getString("update_date");
			return new User(idData, loginIdData, nameData, birthData, passwordData, createData, updataData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

					return null;
				}
			}
		}
	}

	public void update(String name, String birthDate, String password, String id) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			con = DBManager.getConnection();
			String insertSQL = "UPDATE user SET name=?, birth_date=?, password=?, update_date=now() WHERE id=?";

			stmt = con.prepareStatement(insertSQL);

			stmt.setString(1, name);
			stmt.setString(2, birthDate);
			stmt.setString(3, password);
			stmt.setString(4, id);
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void delete(String id) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			con = DBManager.getConnection();
			String deleteSQL = "DELETE FROM user WHERE id=?";
			stmt = con.prepareStatement(deleteSQL);
			stmt.setString(1, id);
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
